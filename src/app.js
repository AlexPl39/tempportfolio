
(function(){
    'use strict';
    angular.module('challengeApp', ['ui.router'])

        .config(function($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.otherwise("/");

            $stateProvider
                .state('main', {
                    url: "/",
                    templateUrl: "src/main/main.html"
                })
                .state('firstExample', {
                    url: "/firstExample",
                    templateUrl: "src/firstExample/firstExample.html",
                    controller: "firstExampleCtrl"
                })
        });
})();