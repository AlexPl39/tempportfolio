(function(){
    'use strict';
    angular.module('challengeApp').component('ticTacToe', {
        bindings: {
            move: '='
        },
        templateUrl: "./src/firstExample/ticTacToe.html",
        controller: "firstExampleCtrl"
    });

})();