(function(){
    'use strict';
    angular.module('challengeApp').controller('firstExampleCtrl', function($scope) {
        var gameEnded = false

        $scope.move = "O"

        $scope.gameState = [
            ["", "", ""],
            ["", "", ""],
            ["", "", ""]
        ]

        $scope.stateClass = [
            ["", "", ""],
            ["", "", ""],
            ["", "", ""]
        ]

        $scope.reset = function () {
            gameEnded = false
            $scope.move = "O"
            $scope.gameState = [
                ["", "", ""],
                ["", "", ""],
                ["", "", ""]
            ]
            $scope.stateClass = [
                ["", "", ""],
                ["", "", ""],
                ["", "", ""]
            ]
        }


        $scope.moveAction = function(state){
            if(!gameEnded){
                var row = state[0],
                    column = state[1],
                    current = $scope.gameState[row][column]
                if(!current){
                    $scope.gameState[row][column] = $scope.move
                    if($scope.move == "O"){
                        $scope.move = "X"
                    }
                    else{
                        $scope.move = "O"
                    }
                }
                else{
                    $scope.gameState[row][column] = ""
                }

                checkEndState($scope.gameState)
            }
        }

       var checkEndState = function (gameState) {

           for (var stateRow = 0; stateRow < 3; stateRow++) {
               for (var state = 0; state < 3; state++) {
                   // TODO: finish game state check
                   // horizontal
                   if (state == 0 && gameState[stateRow][state]){
                       var firstCol = gameState[stateRow][state] || false,
                           secondCol = gameState[stateRow][state + 1] || false,
                           thirdCol = gameState[stateRow][state + 2] || false
                       if(firstCol == secondCol && secondCol == thirdCol){
                           console.log('horizontal win')
                           $scope.stateClass[stateRow][state] = "win"
                           $scope.stateClass[stateRow][state + 1] = "win"
                           $scope.stateClass[stateRow][state + 2] = "win"
                           gameEnded = true
                       }
                   }
                   // vertical
                   if (stateRow == 0 && gameState[stateRow][state]){
                       var firstCol = gameState[stateRow][state] || false,
                           secondCol = gameState[stateRow + 1][state] || false,
                           thirdCol = gameState[stateRow + 2][state] || false
                       if(firstCol == secondCol && secondCol == thirdCol){
                           console.log('vertical win')
                           $scope.stateClass[stateRow][state] = "win"
                           $scope.stateClass[stateRow + 1][state] = "win"
                           $scope.stateClass[stateRow + 2][state] = "win"
                           gameEnded = true
                       }
                   }
                   // diagonal
                   if (state == 0 && stateRow == 0 && gameState[stateRow][state]){
                       var firstCol = gameState[stateRow][state] || false,
                           secondCol = gameState[stateRow + 1][state + 1] || false,
                           thirdCol = gameState[stateRow + 2][state + 2] || false
                       if(firstCol == secondCol && secondCol == thirdCol){
                           console.log('diagonal win')
                           $scope.stateClass[stateRow][state] = "win"
                           $scope.stateClass[stateRow + 1][state + 1] = "win"
                           $scope.stateClass[stateRow + 2][state + 2] = "win"
                           gameEnded = true
                       }
                   }
                   // back diagonal
                   if (state == 2 && stateRow == 0 && gameState[stateRow][state]){
                       var firstCol = gameState[stateRow][state] || false,
                           secondCol = gameState[stateRow + 1][state - 1] || false,
                           thirdCol = gameState[stateRow + 2][state - 2] || false
                       if(firstCol == secondCol && secondCol == thirdCol){
                           console.log('back diagonal win')
                           $scope.stateClass[stateRow][state] = "win"
                           $scope.stateClass[stateRow + 1][state - 1] = "win"
                           $scope.stateClass[stateRow + 2][state - 2] = "win"
                           gameEnded = true
                       }
                   }
               }
           }
       }

    });

})();